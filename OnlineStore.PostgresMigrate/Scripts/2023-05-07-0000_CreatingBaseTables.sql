﻿CREATE TABLE public."Category" (
  "Id" SERIAL PRIMARY KEY,
  "Name" TEXT NOT NULL,
  "Description" TEXT
);

CREATE TABLE public."Product" (
  "Id" SERIAL PRIMARY KEY,
  "Name" TEXT NOT NULL,
  "Description" TEXT,
  "Price" DECIMAL NOT NULL,
  "CategoryId" INTEGER REFERENCES "Category"("Id") ON DELETE SET NULL
);

CREATE TABLE public."User" (
  "Id" SERIAL PRIMARY KEY,
  "Name" TEXT NOT NULL,
  "Email" TEXT NOT NULL,
  "Password" TEXT NOT NULL
);

CREATE TABLE public."Order" (
  "Id" SERIAL PRIMARY KEY,
  "OrderDate" TIMESTAMP NOT NULL DEFAULT now(),
  "TotalAmount" DECIMAL NOT NULL,
  "UserId" INTEGER REFERENCES "User"("Id") ON DELETE SET NULL
);

CREATE TABLE public."OrderDetails" (
  "Id" SERIAL PRIMARY KEY,
  "Quantity" INTEGER NOT NULL,
  "Price" DECIMAL NOT NULL,
  "ProductId" INTEGER REFERENCES "Product"("Id") ON DELETE SET NULL,
  "OrderId" INTEGER REFERENCES "Order"("Id") ON DELETE SET NULL
);

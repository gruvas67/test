# OnlineStore (в данный момент практически ничего не реализовано)

Этот проект сфокусирован на разработке backend-части для онлайн-магазина. Здесь вы найдете код, реализующий различные функциональные возможности онлайн-магазина на C# ASP.NET Core MVC, такие как авторизация пользователей, управление категориями и товарами, обработка заказов и многое другое.

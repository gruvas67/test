﻿namespace OnlineStore.Domain.Models
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal TotalAmount { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }
    }
}

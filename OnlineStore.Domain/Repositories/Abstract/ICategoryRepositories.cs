﻿using OnlineStore.Domain.Models;

namespace OnlineStore.Domain.Repositories.Abstract
{
    public interface ICategoryRepositories
    {
        List<Category> GetAllCategories();
    }
}

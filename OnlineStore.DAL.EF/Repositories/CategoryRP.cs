﻿using OnlineStore.Domain.Repositories.Abstract;
using OnlineStore.Domain.Models;




namespace OnlineStore.DAL.EF.Repositories
{
    public class CategoryRP : ICategoryRepositories
    {
        private readonly PostgreeContext _context;

        public CategoryRP(PostgreeContext context)
        {
            _context = context;
        }

        List<Category> ICategoryRepositories.GetAllCategories()
        {
            return _context.Set<Category>().ToList();
        }
    }
}
